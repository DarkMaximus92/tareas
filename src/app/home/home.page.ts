import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  texto = "Listado de personas";
  personas: string[] = ["Maria", "Juan", "Fabian"];

  constructor() {}

  changeText(){
    this.texto = "Texto cambiado";
  }

}
