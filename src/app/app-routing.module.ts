import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SecondComponent } from './second/second.component';
import { Clase3Component } from './clase3/clase3.component';
import { Clase3bComponent } from './clase3b/clase3b.component';
import { Clase3cComponent } from './clase3c/clase3c.component';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { TiendaComponent } from './tienda/tienda.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path:'second',
    component:SecondComponent
  },
  {
    path:'clase3',
    component:Clase3Component
  },
  {
    path:'clase3b',
    component:Clase3bComponent
  },
  {
    path:'clase3c',
    component:Clase3cComponent
  },
  {
    path:'calculadora',
    component:CalculadoraComponent
  },
  {
    path:'tienda',
    component:TiendaComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
