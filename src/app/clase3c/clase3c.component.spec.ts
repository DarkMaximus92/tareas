import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Clase3cComponent } from './clase3c.component';

describe('Clase3cComponent', () => {
  let component: Clase3cComponent;
  let fixture: ComponentFixture<Clase3cComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clase3cComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Clase3cComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
