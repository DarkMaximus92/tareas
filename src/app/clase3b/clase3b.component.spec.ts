import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Clase3bComponent } from './clase3b.component';

describe('Clase3bComponent', () => {
  let component: Clase3bComponent;
  let fixture: ComponentFixture<Clase3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clase3bComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Clase3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
